# 3CB Arma3Sync Documentation

## A3Sync GUI

* Start MobaXterm
* Select Arma3Sync

![a3sync_001](a3sync_001.jpg)

* Enter password

![a3sync_002](a3sync_002.jpg)

* Answer the yes/no question (your choice!)

![a3sync_003](a3sync_003.jpg)

* Press 1 and Enter

![a3sync_029](a3sync_029.jpg)

* Wait for the GUI (it will open in a new window)

![a3sync_005](a3sync_005.jpg)

## Open Repository

* Select **Repository** tab

![a3sync_006](a3sync_006.jpg)

* Select the **3CB Public A3Sync Repository**
* And click **Connect to Repository** (the Button with the blue arrow on the right side)

![a3sync_007](a3sync_007.jpg)

### Update Repository
Example use case: Updated a mod per FTP


* In the new opened Tab select **Repository** (The button with the cogwheel on the right side)

![a3sync_008](a3sync_008.jpg)

* Click **Build repository**

![a3sync_009](a3sync_009.jpg)

* Wait for it to finish
  (As more files are chaneged as longer the build will take)

* Press OK

![a3sync_011](a3sync_011.jpg)

* Click the Changelog **View** Button to validate the changes

![a3sync_012](a3sync_012.jpg)

![a3sync_013](a3sync_013.jpg)


### Add new Modset & Server

* Click the **Events** Button (The Button with the pen)

![a3sync_014](a3sync_014.jpg)

* Add a new Event with **+** Button

![a3sync_015](a3sync_015.jpg)

* Enter the name and description

![a3sync_016](a3sync_016.jpg)

* Select the new Modset
* Select the Mods

![a3sync_017](a3sync_017.jpg)

* Go to the **Online** Tab

![a3sync_018](a3sync_018.jpg)

* Add a new Server with the **+** Button

![a3sync_019](a3sync_019.jpg)

* Enter the server information

![a3sync_021](a3sync_021.jpg)

* Select the Modset/Event

![a3sync_022](a3sync_022.jpg)

* Switch back to the **3CB Public A3Sync Repository** Tab
* And click the **Repository** Button (Cogwheel)

![a3sync_023](a3sync_023.jpg)

* Click **Options**

![a3sync_024](a3sync_024.jpg)

* Enable the Server and hit OK

![a3sync_026](a3sync_026.jpg)

* **Build** the repository

![a3sync_028](a3sync_028.jpg)